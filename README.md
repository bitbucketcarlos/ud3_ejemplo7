# Ud3_Ejemplo7
_Ejemplo 7 de la Unidad 3._ 

Vamos a ver cómo salvar el estado de una Actividad. Guardaremos el valor de un contador que irá aumentado en 1 cada vez que se 
pulse el botón. Para salvar su estado usaremos un par de clave/valor.

Haremos la prueba rotando la pantalla y veremos cómo el valor del contador no se habrá perdido. También podremos observar cómo, 
si escribimos en el _EditText_ y rotamos, su valor no se pierde, ya que por defecto Android salva el estado de todos los elementos 
que tienen un _id_ único (otra razón de la importancia de utilizar _ids_).

Para ello primero nos creamos el _layout_, con un _EditText_, un botón y un _TextView_.

_activity_main.xml_:
```html
<androidx.constraintlayout.widget.ConstraintLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <EditText
        android:id="@+id/editText"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_alignParentTop="true"
        android:layout_alignParentStart="true"
        app:layout_constraintLeft_toLeftOf="parent"
        app:layout_constraintRight_toRightOf="parent"
        app:layout_constraintTop_toTopOf="parent"
        app:layout_constraintBottom_toTopOf="@+id/boton"/>

    <Button
        android:id="@+id/boton"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_centerInParent="true"
        android:text="@string/contar"
        app:layout_constraintLeft_toLeftOf="parent"
        app:layout_constraintRight_toRightOf="parent"
        app:layout_constraintTop_toTopOf="parent"
        app:layout_constraintBottom_toBottomOf="parent"/>

    <TextView
        android:id="@+id/textViewContador"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_below="@id/boton"
        app:layout_constraintLeft_toLeftOf="parent"
        app:layout_constraintRight_toRightOf="parent"
        app:layout_constraintTop_toBottomOf="@id/boton"
        app:layout_constraintBottom_toBottomOf="parent"/>

</androidx.constraintlayout.widget.ConstraintLayout>
```
Y en la clase _MainActivity_ vamos a:

*    Crear una constante para la clave y un atributo para el valor.
*    Sobrescribir las funciones _onSaveInstanceState_ para salvar el estado del contador y _onRestoreInstanceState_ para restaurarlo.


```java
class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    companion object {
        const val CLAVE: String = "CONTADOR" // Nombre de la clave.
    }

    private var cont: Int = 0 // Valor de la clave.

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root

        setContentView(view)

        binding.boton.setOnClickListener {
            cont++
            binding.textViewContador.text = "Contador: " + cont
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        // Guardamos el estado usando el par clave/valor
        // Usamos putInt ya que cont es entero.
        outState.putInt(CLAVE, cont)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)

        // Recuperamos el valor en función de su clave.
        // Usamos getInt ya que cont es entero.
        cont = savedInstanceState.getInt(CLAVE)

        // Al girar seguimos mostrando el valor del TextView
        binding.textViewContador.text = "Contador: " + cont
    }
}
```
