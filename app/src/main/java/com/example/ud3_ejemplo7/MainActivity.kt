package com.example.ud3_ejemplo7

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.ud3_ejemplo7.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    companion object {
        const val CLAVE: String = "CONTADOR" // Nombre de la clave.
    }

    private var cont: Int = 0 // Valor de la clave.

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root

        setContentView(view)

        binding.boton.setOnClickListener {
            cont++
            binding.textViewContador.text = "Contador: " + cont
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        // Guardamos el estado usando el par clave/valor
        // Usamos putInt ya que cont es entero.
        outState.putInt(CLAVE, cont)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)

        // Recuperamos el valor en función de su clave.
        // Usamos getInt ya que cont es entero.
        cont = savedInstanceState.getInt(CLAVE)

        // Al girar seguimos mostrando el valor del TextView
        binding.textViewContador.text = "Contador: " + cont
    }
}

